
module.exports = (sequelize, Sequelize) => {
    const Dictionary = sequelize.define("dictionary", {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: Sequelize.INTEGER,
            allowNull: false
        },

        name: {
            type: Sequelize.STRING(300),
            allowNull: false
        },
        phone_number: {
            type: Sequelize.STRING(20),
            allowNull: false
        },
        email: {
            type: Sequelize.STRING(100),
            allowNull: false,
            validate: {
                isEmail: true,
            }
        },
        website: {
            type: Sequelize.STRING(100),
            allowNull: false,
        },
        is_favorite: {
            type: Sequelize.INTEGER(2),
            allowNull: false,
            defaultValue: 0,
        },
        status: {
            type: Sequelize.INTEGER(2),
            defaultValue: 1,
            allowNull: false,
        },

    });

    return Dictionary;
};