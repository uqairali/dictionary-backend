const { authJwt } = require("../middleware");
const controller = require("../controllers/dictionary");

module.exports = function (app) {
    app.post("/api/dictionary/newEntry", [authJwt.verifyToken], controller.newEntry);
    app.get("/api/dictionary/findAll", [authJwt.verifyToken], controller.findAll);
    app.put("/api/dictionary/update/:id", [authJwt.verifyToken], controller.updateRecord);
    app.delete("/api/dictionary/delete/:id", [authJwt.verifyToken], controller.deleteRecord);
};
