const { authJwt } = require("../middleware");
const controller = require("../controllers/user");

module.exports = function (app) {
    app.post("/api/user/register", controller.registerUser);
    app.post("/api/user/login", controller.userLogin);
};
