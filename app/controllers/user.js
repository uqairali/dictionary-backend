const db = require("../models");
const config = require("../../config/auth.config");
const User = db.user;
var jwt = require("jsonwebtoken");
var { encrypt, decrypt } = require('../utils/helper');

exports.registerUser = async (req, res) => {
    try {
        console.log(req.body)
        if (!req.body.email) {
            res.status(400).json({
                message: "Email can not be empty!"
            });
            return;
        }
        if (!req.body.password) {
            res.status(400).json({
                message: "Password can not be empty!"
            });
            return;
        }
        const data = {
            name: req.body.name,
            email: req.body.email,
            password: encrypt(req.body.password),
        };
        var findDuplicate = await User.findOne({
            where: {
                email: data.email
            }
        })
        if (findDuplicate) {
            return res.status(400).json({ message: "Email already exists" })
        }
        var result = await User.create(data)
        res.status(200).json(result)
    } catch (err) {
        res.status(500).json({ message: err.message });
    }

};

exports.userLogin = async (req, res) => {
    try {
        var result = await User.findOne({
            where: {
                email: req.body.email
            }
        })
        if (!result) {
            return res.status(404).json({ message: "User Not Found" });
        }
        var decryptedPass = await decrypt(result.password)
        if (decryptedPass !== req.body.password) {
            return res.status(401).json({
                accessToken: null,
                message: "Invalid Password"
            });
        }

        var token = jwt.sign({ id: result.id }, config.secret, {
            expiresIn: config.expiresIn
        });


        res.status(200).json({
            user: result,
            accessToken: token
        })
    } catch (err) {
        res.status(500).json({ message: err.message });
    }

}
