const db = require("../models");
const User = db.user;
const Dictionary = db.dictionary;

exports.newEntry = async (req, res) => {
    try {
        var data = req.body;
        data.user_id = req.userId;

        var result = await Dictionary.create(data)
        res.status(200).json(result)
    } catch (err) {
        res.status(500).json({ message: err.message });
    }

};

exports.findAll = async (req, res) => {
    try {
        var result = await Dictionary.findAll({
            where: {
                status: 1,
                user_id: req.userId
            },
            include: [
                {
                    model: User
                },

            ],
        })
        res.status(200).json(result)
    } catch (err) {
        res.status(500).json({ message: err.message });
    }

}

exports.updateRecord = async (req, res) => {
    try {
        var data = req.body;
        var result = await Dictionary.update(
            data,
            { where: { id: req.params.id } }
        )
        if (result[0] === 0)
            res.status(400).json({ message: "Record Not Found" })
        else {
            res.status(200).json({ message: "Record Updated" })
        }
    } catch (err) {
        res.status(500).json({ message: err.message });
    }

}

exports.deleteRecord = async (req, res) => {
    try {
        var result = await Dictionary.destroy(
            { where: { id: req.params.id } }
        )

        res.status(200).json(result)

    } catch (err) {
        res.status(500).json({ message: err.message });
    }

}
