const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();

const db = require("./app/models");

initDB()
async function initDB() {
    try {
        await db.sequelize.sync({ force: false, alter: true })
        console.log('DB connected successfully');
    } catch (err) {
        console.log(err);
    }

}


app.use(cors());


// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));


require('./app/routes/user')(app);
require('./app/routes/dictionary')(app);


app.listen(process.env.PORT || 8080, () => {
    console.log("Server started...");
});